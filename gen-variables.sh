#!/usr/bin/env bash

set -e
#set -x
set -u

# $1 parameter to extract
#    This argument is required
# $2 location of bundle.json
#    This is optional. Defaults to
#    bundle.json in the current
#    directory.
get_from_bundle() {
  bundle_json="bundle.json"
  if [ $# -gt 1 ]; then
    bundle_json="$2"
  fi
  jq -r ".$1" "${bundle_json}"
}

# Generate a file called submit-tests.sh
# to make it easier for other to reuse.
generate_submit_tests() {
  cd "${WORKDIR}/lava-test-plans"
  cat << EOF | tee "${WORKDIR}/submit-tests.sh"
#!/bin/bash -e
[ -d "${WORKDIR}/lava-test-plans" ] && cd "${WORKDIR}/lava-test-plans" || exit 1
python3 submit_for_testing.py \\
  ${DRY_RUN} \\
  --variables "${TEST_PLANS_VAR_FILE}" \\
  --device-type "${DEVICE_TYPE}" \\
  --build-number "${GIT_DESCRIBE}" \\
  --lava-server "${LAVA_SERVER}" \\
  --qa-server "${QA_SERVER}" \\
  --qa-server-team "${QA_TEAM}" \\
  --qa-server-project "${QA_PROJECT}" \\
  ${LAVA_TESTS} \\
  ${testplan_device_path} \\
  ${QA_ENVIRONMENT}
EOF
}

# Given a MACHINE, define DEVICE_TYPE
# $1: MACHINE name. Required
machine_to_devicetype() {
  if [ $# -lt 1 ]; then
    return 1
  fi

  local MACHINE="$1"
  case "${MACHINE}" in
    dragonboard-410c) DEVICE_TYPE="dragonboard-410c" ;;
    hikey)            DEVICE_TYPE="hi6220-hikey" ;;
    intel-core2-32)   DEVICE_TYPE="i386" ;;
    juno)             DEVICE_TYPE="juno-r2" ;;
    ls2088ardb)       DEVICE_TYPE="nxp-ls2088" ;;
    am57xx-evm)       DEVICE_TYPE="x15" ;;
    intel-corei7-64)  DEVICE_TYPE="x86" ;;
    qemu_arm)         DEVICE_TYPE="qemu_arm" ;;
    qemu_arm64)       DEVICE_TYPE="qemu_arm64" ;;
    qemu_i386)        DEVICE_TYPE="qemu_i386" ;;
    qemu_x86_64)      DEVICE_TYPE="qemu_x86_64" ;;
  esac

  export DEVICE_TYPE
}

check_and_write() {
  if [ -v "${1}" ] && [ -n "${1}" ]; then
    echo "${1}=${!1}" >> "${TEST_PLANS_VAR_FILE}"
  fi
}

# Assign a value to a variable if:
# * it doesn't exist, or
# * its value is empty
# $1: Variable name
# $2: Variable value
check_and_set() {
  if [ -v "${1}" ]; then
    if [ -z "${1}" ]; then
      export "${1}"="${2}"
    fi
  else
    export "${1}"="${2}"
  fi
}

# $1: $QA_PROJECT
# $2: $DEVICE_TYPE
is_board_migrated() {
  if [ $# -lt 2 ]; then
    return 1
  fi

  ret="NO"

  this_combo="${1}::${2}"
  if [[ ${MIGRATED[*]} =~ ${this_combo} ]]; then
    ret="YES"
  fi

  echo "${ret}"
}

# Define variables that are used by submit_for_testing.py
define_sft_vars_for_machine() {
  # DEVICE_TYPE is defined by machine_to_devicetype()
  # QA_PROJECT is defined by create_kernel_vars_for_machine()
  [ ! -v GIT_DESCRIBE ] && [ -v LATEST_SHA ] && GIT_DESCRIBE="${LATEST_SHA:0:12}"
  LAVA_SERVER=https://lkft.validation.linaro.org/RPC2/
  QA_SERVER=https://qa-reports.linaro.org
  TEST_PLANS_VAR_FILE="${WORKDIR}/variables.ini"
  check_and_set QA_TEAM "staging-lkft"

  case "${DEVICE_TYPE}" in
    nxp-ls2088)
      LAVA_SERVER=https://lavalab.nxp.com/RPC2/
      ;;
  esac

  export \
    GIT_DESCRIBE \
    LAVA_SERVER \
    QA_SERVER \
    QA_TEAM \
    TEST_PLANS_VAR_FILE
}

create_rootfs_vars_for_machine() {

  BUNDLED_URL=${1}
  ROOTFS_BUNDLE="${BUNDLED_URL}/bundle.json"

  bundle_json="$(download_and_cache "${ROOTFS_BUNDLE}")"
  if [ -n "${bundle_json}" ] && [ -z "${ROOTFS_URL}" ]; then
    case "${ROOTFS_MACHINE}" in
      am57xx-evm)
        case ${MACHINE} in
          am57xx-evm) rootfs_ext="ext4gz" ;;
          juno)       rootfs_ext="tarxz"  ;;
          qemu_arm)   rootfs_ext="ext4gz" ;;
        esac
        ROOTFS_URL="${BUNDLED_URL}/$(get_from_bundle "rootfs.${rootfs_ext}" "${bundle_json}")"
        ;;

      dragonboard-410c)
        ROOTFS_URL="${BUNDLED_URL}/$(get_from_bundle rootfs.ext4gz "${bundle_json}")"
        ;;

      hikey)
        ROOTFS_URL="${BUNDLED_URL}/$(get_from_bundle rootfs.ext4gz "${bundle_json}")"
        ;;

      intel-core2-32)
        case ${MACHINE} in
          intel-core2-32) rootfs_ext="tarxz" ;;
          qemu_i386)      rootfs_ext="ext4gz" ;;
        esac
        ROOTFS_URL="${BUNDLED_URL}/$(get_from_bundle "rootfs.${rootfs_ext}" "${bundle_json}")"
        ;;

      intel-corei7-64)
        case ${MACHINE} in
          intel-corei7-64) rootfs_ext="tarxz" ;;
          qemu_x86_64)     rootfs_ext="ext4gz" ;;
        esac
        ROOTFS_URL="${BUNDLED_URL}/$(get_from_bundle "rootfs.${rootfs_ext}" "${bundle_json}")"
        ;;

      juno)
        case ${MACHINE} in
          juno)       rootfs_ext="tarxz" ;;
          qemu_arm64) rootfs_ext="ext4gz" ;;
        esac
        # shellcheck disable=SC2034
        ROOTFS_URL="${BUNDLED_URL}/$(get_from_bundle "rootfs.${rootfs_ext}" "${bundle_json}")"
        ;;

      ls2088ardb)
        ROOTFS_URL="${BUNDLED_URL}/$(get_from_bundle rootfs.tarxz "${bundle_json}")"
        ;;
    esac
  fi

  for var in \
    ROOTFS_URL; do
    check_and_write ${var}
  done

}

create_kernel_vars_for_machine() {
  echo
  echo "====================================================="
  echo "Now submitting jobs for ${MACHINE^^}"
  echo "Using ${build_json} for build info"

  KERNEL_NAME=Image
  unset DTB_FILENAME
  BUNDLED_URL=${1}
  BOOT_BUNDLE="${BUNDLED_URL}/bundle.json"

  if [ -v GITLAB_CI ]; then
    if [ ! -f "${build_json}" ]; then
      echo "Artifact ${build_json} not found. Can't continue."
      exit 1
    fi
    DOWNLOAD_URL="$(jq -r .download_url "${build_json}")"
    # The URL ends with /, so remove the last one
    KERNEL_PUB_DEST="$(echo "${DOWNLOAD_URL}" | cut -d/ -f1-4)"
    BUILD_URL="${CI_PIPELINE_URL}"
    BUILD_NUMBER="${CI_BUILD_ID}"
    GIT_DESCRIBE="$(jq -r .git_describe "${build_json}")"
    build_definition_json="$(download_and_cache "${DOWNLOAD_URL}build_definition.json")"
    MAKE_KERNELVERSION="$(jq -r .kernel_version "${build_definition_json}")"

    case "${REPO_NAME}" in
      mainline)
        check_and_set QA_PROJECT "linux-mainline-oe"
        SKIPGEN_KERNEL_VERSION="${GIT_BRANCH}"
        ;;
      next)
        check_and_set QA_PROJECT "linux-next-oe"
        SKIPGEN_KERNEL_VERSION="default"
        ;;
      stable-rc)
        major_minor_version=$(echo "${GIT_BRANCH#linux-*}" | cut -d. -f1-2)
        check_and_set QA_PROJECT "linux-stable-rc-${major_minor_version}-oe"
        SKIPGEN_KERNEL_VERSION="${GIT_BRANCH}"
        ;;
      *)
        # Consider: Do we maybe need a "people" project?
        check_and_set QA_PROJECT "linux-mainline-oe"
        SKIPGEN_KERNEL_VERSION="default"
        ;;
    esac

    # If there is a QA_PROJECT suffix, apply it
    check_and_set QA_PROJECT_SUFFIX ""
    QA_PROJECT="${QA_PROJECT}${QA_PROJECT_SUFFIX}"
  fi

  bundle_json="$(download_and_cache "${BOOT_BUNDLE}")"
  if [ -n "${bundle_json}" ] && [ -z "${BOOT_URL}" ]; then
    case "${ROOTFS_MACHINE}" in
      dragonboard-410c)
        BOOT_URL="${BUNDLED_URL}/$(get_from_bundle boot "${bundle_json}")"
        ;;

      hikey)
        BOOT_URL="${BUNDLED_URL}/$(get_from_bundle boot "${bundle_json}")"
        ;;

    esac
  fi
  case "${MACHINE}" in
    dragonboard-410c)
      # Qualcomm's Dragonboard 410c
      DTB_FILENAME=dtbs/qcom/apq8016-sbc.dtb
      ;;
    hikey)
      # HiKey
      DTB_FILENAME=dtbs/hisilicon/hi6220-hikey.dtb
      ;;
    juno | qemu_arm64)
      # Arm's Juno
      DTB_FILENAME=dtbs/arm/juno-r2.dtb
      if [ "${GIT_BRANCH}" = "linux-4.4.y" ]; then
        DTB_FILENAME=dtbs/arm/juno-r1.dtb
      fi
      if [ "${MACHINE}" = "qemu_arm64" ]; then
        unset DTB_FILENAME
        KERNEL_URL=${KERNEL_PUB_DEST}/${KERNEL_NAME}
        BOOT_URL=${KERNEL_URL}
      fi
      ;;
    ls2088ardb)
      # NXP's LS2088A RDB
      DTB_FILENAME=dtbs/freescale/fsl-ls2088a-rdb.dtb
      ;;
    am57xx-evm | qemu_arm)
      # am57xx-evm
      KERNEL_NAME=zImage
      DTB_FILENAME=dtbs/am57xx-beagle-x15.dtb
      KERNEL_URL=${KERNEL_PUB_DEST}/${KERNEL_NAME}
      BOOT_URL=${KERNEL_URL}
      # shellcheck disable=SC2034
      if [ "${MACHINE}" = "qemu_arm" ]; then
        unset DTB_FILENAME
      fi
      ;;
    intel-corei7-64 | qemu_x86_64)
      # intel-corei7-64
      KERNEL_NAME=bzImage
      KERNEL_URL=${KERNEL_PUB_DEST}/${KERNEL_NAME}
      BOOT_URL=${KERNEL_URL}
      ;;
    intel-core2-32 | qemu_i386)
      # intel-core2-32
      KERNEL_NAME=bzImage
      KERNEL_URL=${KERNEL_PUB_DEST}/${KERNEL_NAME}
      # shellcheck disable=SC2034
      BOOT_URL=${KERNEL_URL}
      ;;
  esac

  KERNEL_URL=${KERNEL_PUB_DEST}/${KERNEL_NAME}

  if [[ $DEVICE_TYPE == *"qemu_"* ]]; then
    # shellcheck disable=SC2034
    OVERLAY_MODULES_URL=${KERNEL_PUB_DEST}/modules.tar.xz
  else
    # shellcheck disable=SC2034
    MODULES_URL=${KERNEL_PUB_DEST}/modules.tar.xz
  fi

  cat << EOF >> "${TEST_PLANS_VAR_FILE}"
DEVICE_TYPE=${DEVICE_TYPE}
KERNEL_PUB_DEST=${KERNEL_PUB_DEST}
BUILD_NUMBER=${BUILD_NUMBER}
BUILD_URL=${BUILD_URL}
KERNEL_URL=${KERNEL_URL}
KERNEL_CONFIG_URL=${KERNEL_PUB_DEST}/kernel.config
#
KERNEL_DESCRIBE=${GIT_DESCRIBE}
KERNEL_COMMIT=${LATEST_SHA}
MAKE_KERNELVERSION=${MAKE_KERNELVERSION}
SKIPGEN_KERNEL_VERSION=${SKIPGEN_KERNEL_VERSION}
KERNEL_BRANCH=${GIT_BRANCH}
KERNEL_REPO=${GIT_REPO}
EOF

  if [ -v DTB_FILENAME ] && [ -n "${DTB_FILENAME}" ]; then
    echo "DTB_URL=${KERNEL_PUB_DEST}/${DTB_FILENAME}" >> "${TEST_PLANS_VAR_FILE}"
  fi

  for var in \
    BOOT_URL \
    DTB_FILENAME \
    MODULES_URL \
    OVERLAY_MODULES_URL \
    LAVA_TEST_NAME_SUFFIX; do
    check_and_write ${var}
  done

}

project_specific_variables() {
  cat << EOF >> "${TEST_PLANS_VAR_FILE}"
# hikey:
# common, need adjustment per project:
PROJECT_NAME=${QA_PROJECT}
KSELFTEST_PATH=/opt/kselftests/mainline/
LAVA_JOB_PRIORITY=${LAVA_JOB_PRIORITY}
TDEFINITIONS_REVISION=master
PROJECT=projects/lkft/
TAGS=${LAVA_TAGS}
KVM_UNIT_TESTS_REVISION=f2606a873e805f9aff4c4879ec75e65d7e30af73
EOF
}

# $1: Branch, in the format "linux-4.14.y", "mainline", "next". Required.
# $2: DEVICE_TYPE. Optional.
# $3: QA_PROJECT. Optional.
# From the environment:
#   LAVA_JOB_PRIORITY: If defined, it will return this value.
get_branch_priority() {
  local branch=""
  local device_type=""
  local qa_project=""

  # Can be overridden via environment
  if [ -v LAVA_JOB_PRIORITY ]; then
    echo "${LAVA_JOB_PRIORITY}"
    return
  fi

  if [ $# -lt 1 ]; then
    return 1
  fi

  branch=$1

  if [ $# -eq 3 ]; then
    device_type="$2"
    qa_project="$3"
  fi

  prio=25

  case ${branch} in
    mainline | next) prio=35 ;;
    linux-4.4.y)     prio=71 ;;
    linux-4.9.y)     prio=72 ;;
    linux-4.14.y)    prio=73 ;;
    linux-4.19.y)    prio=74 ;;
    linux-5.4.y)     prio=75 ;;
    linux-5.5.y)     prio=77 ;;
    linux-5.6.y)     prio=78 ;;
    linux-5.7.y)     prio=78 ;;
  esac

  # If the board/branch combination is not in production, reduce priority
  is_migrated="$(is_board_migrated "${qa_project}" "${device_type}")"
  if [ ! "${is_migrated}" = "YES" ]; then
    prio=$((prio - 10))
  fi

  echo ${prio}
}

# Get the hash of any string
hash_string() {
  echo -n "$1" | sha1sum | cut -c1-40
}

# $1: URL to download and cache
# From the environment:
#   WORKSPACE
#
# Returns the absolute path and file name of the downloaded resource.
download_and_cache() {
  hashed_url="$(hash_string "${1}")"
  output_file="${WORKDIR}/.downloads/${hashed_url}"

  mkdir -p "${WORKDIR}/.downloads/"

  if [ ! -s "${output_file}" ]; then
    wget -O "${output_file}" "${1}"
  fi
  if [ -s "${output_file}" ]; then
    readlink -e "${output_file}"
  fi
}

# START HERE

# This is the list of QA_PROJECT::LAVA_DEVICE combinations
# that have been migrated from LKFT 1.0 to LKFT 2.0
MIGRATED=(
  linux-mainline-oe::dragonboard-410c
  linux-next-oe::dragonboard-410c
  linux-stable-rc-4.9-oe::dragonboard-410c
  linux-stable-rc-4.14-oe::dragonboard-410c
  linux-stable-rc-4.19-oe::dragonboard-410c
  linux-stable-rc-5.4-oe::dragonboard-410c
  linux-stable-rc-5.5-oe::dragonboard-410c
  linux-stable-rc-5.6-oe::dragonboard-410c
  linux-stable-rc-5.7-oe::dragonboard-410c
  linux-mainline-oe::hi6220-hikey
  linux-next-oe::hi6220-hikey
  linux-stable-rc-4.9-oe::hi6220-hikey
  linux-stable-rc-4.14-oe::hi6220-hikey
  linux-stable-rc-4.19-oe::hi6220-hikey
  linux-stable-rc-5.4-oe::hi6220-hikey
  linux-stable-rc-5.5-oe::hi6220-hikey
  linux-stable-rc-5.6-oe::hi6220-hikey
  linux-stable-rc-5.7-oe::hi6220-hikey
  linux-mainline-oe::juno-r2
  linux-mainline-oe-sanity::juno-r2
  linux-next-oe::juno-r2
  linux-next-oe-sanity::juno-r2
  linux-stable-rc-4.4-oe::juno-r2
  linux-stable-rc-4.4-oe-sanity::juno-r2
  linux-stable-rc-4.9-oe::juno-r2
  linux-stable-rc-4.9-oe-sanity::juno-r2
  linux-stable-rc-4.14-oe::juno-r2
  linux-stable-rc-4.14-oe-sanity::juno-r2
  linux-stable-rc-4.19-oe::juno-r2
  linux-stable-rc-4.19-oe-sanity::juno-r2
  linux-stable-rc-5.4-oe::juno-r2
  linux-stable-rc-5.4-oe-sanity::juno-r2
  linux-stable-rc-5.5-oe::juno-r2
  linux-stable-rc-5.5-oe-sanity::juno-r2
  linux-stable-rc-5.6-oe::juno-r2
  linux-stable-rc-5.6-oe-sanity::juno-r2
  linux-stable-rc-5.7-oe::juno-r2
  linux-stable-rc-5.7-oe-sanity::juno-r2
  linux-mainline-oe::nxp-ls2088
  linux-stable-rc-4.9-oe::nxp-ls2088
  linux-stable-rc-4.14-oe::nxp-ls2088
  linux-stable-rc-4.19-oe::nxp-ls2088
  linux-stable-rc-5.4-oe::nxp-ls2088
  linux-stable-rc-5.5-oe::nxp-ls2088
  linux-stable-rc-5.6-oe::nxp-ls2088
  linux-stable-rc-5.7-oe::nxp-ls2088
  linux-mainline-oe::i386
  linux-mainline-oe-sanity::i386
  linux-next-oe::i386
  linux-next-oe-sanity::i386
  linux-stable-rc-4.4-oe::i386
  linux-stable-rc-4.4-oe-sanity::i386
  linux-stable-rc-4.9-oe::i386
  linux-stable-rc-4.9-oe-sanity::i386
  linux-stable-rc-4.14-oe::i386
  linux-stable-rc-4.14-oe-sanity::i386
  linux-stable-rc-4.19-oe::i386
  linux-stable-rc-4.19-oe-sanity::i386
  linux-stable-rc-5.4-oe::i386
  linux-stable-rc-5.4-oe-sanity::i386
  linux-stable-rc-5.5-oe::i386
  linux-stable-rc-5.5-oe-sanity::i386
  linux-stable-rc-5.6-oe::i386
  linux-stable-rc-5.6-oe-sanity::i386
  linux-stable-rc-5.7-oe::i386
  linux-stable-rc-5.7-oe-sanity::i386
  linux-mainline-oe-sanity::x15
  linux-mainline-oe::x15
  linux-next-oe-sanity::x15
  linux-next-oe::x15
  linux-stable-rc-4.4-oe-sanity::x15
  linux-stable-rc-4.4-oe::x15
  linux-stable-rc-4.9-oe-sanity::x15
  linux-stable-rc-4.9-oe::x15
  linux-stable-rc-4.14-oe-sanity::x15
  linux-stable-rc-4.14-oe::x15
  linux-stable-rc-4.19-oe-sanity::x15
  linux-stable-rc-4.19-oe::x15
  linux-stable-rc-5.4-oe-sanity::x15
  linux-stable-rc-5.4-oe::x15
  linux-stable-rc-5.5-oe-sanity::x15
  linux-stable-rc-5.5-oe::x15
  linux-stable-rc-5.6-oe-sanity::x15
  linux-stable-rc-5.6-oe::x15
  linux-stable-rc-5.7-oe-sanity::x15
  linux-stable-rc-5.7-oe::x15
  linux-mainline-oe-sanity::x86
  linux-mainline-oe::x86
  linux-next-oe-sanity::x86
  linux-next-oe::x86
  linux-stable-rc-4.4-oe-sanity::x86
  linux-stable-rc-4.4-oe::x86
  linux-stable-rc-4.9-oe-sanity::x86
  linux-stable-rc-4.9-oe::x86
  linux-stable-rc-4.14-oe-sanity::x86
  linux-stable-rc-4.14-oe::x86
  linux-stable-rc-4.19-oe-sanity::x86
  linux-stable-rc-4.19-oe::x86
  linux-stable-rc-5.4-oe-sanity::x86
  linux-stable-rc-5.4-oe::x86
  linux-stable-rc-5.5-oe-sanity::x86
  linux-stable-rc-5.5-oe::x86
  linux-stable-rc-5.6-oe-sanity::x86
  linux-stable-rc-5.6-oe::x86
  linux-stable-rc-5.7-oe-sanity::x86
  linux-stable-rc-5.7-oe::x86
  linux-mainline-oe::qemu_arm
  linux-next-oe::qemu_arm
  linux-stable-rc-4.9-oe::qemu_arm
  linux-stable-rc-4.14-oe::qemu_arm
  linux-stable-rc-4.19-oe::qemu_arm
  linux-stable-rc-5.4-oe::qemu_arm
  linux-stable-rc-5.5-oe::qemu_arm
  linux-stable-rc-5.6-oe::qemu_arm
  linux-stable-rc-5.7-oe::qemu_arm
  linux-mainline-oe::qemu_arm64
  linux-next-oe::qemu_arm64
  linux-stable-rc-4.9-oe::qemu_arm64
  linux-stable-rc-4.14-oe::qemu_arm64
  linux-stable-rc-4.19-oe::qemu_arm64
  linux-stable-rc-5.4-oe::qemu_arm64
  linux-stable-rc-5.5-oe::qemu_arm64
  linux-stable-rc-5.6-oe::qemu_arm64
  linux-stable-rc-5.7-oe::qemu_arm64
  linux-mainline-oe::qemu_i386
  linux-next-oe::qemu_i386
  linux-stable-rc-4.9-oe::qemu_i386
  linux-stable-rc-4.14-oe::qemu_i386
  linux-stable-rc-4.19-oe::qemu_i386
  linux-stable-rc-5.4-oe::qemu_i386
  linux-stable-rc-5.5-oe::qemu_i386
  linux-stable-rc-5.6-oe::qemu_i386
  linux-stable-rc-5.7-oe::qemu_i386
  linux-mainline-oe::qemu_x86_64
  linux-next-oe::qemu_x86_64
  linux-stable-rc-4.9-oe::qemu_x86_64
  linux-stable-rc-4.14-oe::qemu_x86_64
  linux-stable-rc-4.19-oe::qemu_x86_64
  linux-stable-rc-5.4-oe::qemu_x86_64
  linux-stable-rc-5.5-oe::qemu_x86_64
  linux-stable-rc-5.6-oe::qemu_x86_64
  linux-stable-rc-5.7-oe::qemu_x86_64
)

if [[ -v CI && ! ${CI_PIPELINE_SOURCE} == "merge_request_event" ]]; then
  env | sort
  echo
fi

LAVA_TESTS=${LAVA_TESTS:-"--test-plan lkft-sanity"}
testplan_device_path="--testplan-device-path projects/lkft/devices"

if [ -v DRY_RUN ]; then
  DRY_RUN="--dry-run"
else
  DRY_RUN=""
fi
export DRY_RUN

if [ -v CI ]; then
  WORKDIR="${CI_PROJECT_DIR}"
else
  WORKDIR="$(dirname "$(readlink -e "$0")")/workspace"
fi
mkdir -p "${WORKDIR}"

rm -rf "${WORKDIR}/lava-test-plans"
if [ ! -d "${WORKDIR}/lava-test-plans" ]; then
  git clone -o origin https://github.com/roxell/lava-test-plans "${WORKDIR}/lava-test-plans"
else
  if [ -v GIT_SYNC ] || [ -v CI ]; then
    (cd "${WORKDIR}/lava-test-plans" && git fetch origin && git reset --hard origin/master)
  fi
fi

echo "lava-test-plans revision $(git -C "${WORKDIR}/lava-test-plans/" rev-parse HEAD)"

if [ -v CI ]; then
  pip install -r "${WORKDIR}/lava-test-plans/requirements.txt"
fi

if [ -v MACHINE ]; then
  # Define MACHINE and DEVICE_TYPE
  machine_to_devicetype "${MACHINE}"

  case "${MACHINE}" in
    qemu_arm)    ROOTFS_MACHINE="am57xx-evm" ;;
    qemu_arm64)  ROOTFS_MACHINE="juno" ;;
    qemu_i386)   ROOTFS_MACHINE="intel-core2-32" ;;
    qemu_x86_64) ROOTFS_MACHINE="intel-corei7-64" ;;
  esac

  # Variables that can be overrided...
  # =======================================================================
  check_and_set ROOTFS_MACHINE "${MACHINE}"
  check_and_set BOOT_URL ""
  check_and_set ROOTFS_URL ""
  check_and_set LAVA_TAGS "production"
  if [ ! -v QA_ENVIRONMENT ]; then
    QA_ENVIRONMENT=""
  else
    QA_ENVIRONMENT="--environment ${QA_ENVIRONMENT}"
  fi
  # =======================================================================

  build_json="build.json"
  if [ $# -ge 1 ]; then
    build_json=$1
  fi

  # Define variables for submit_for_testing
  define_sft_vars_for_machine
  rm -f "${TEST_PLANS_VAR_FILE}"

  BUNDLED_URL="https://storage.lkft.org/rootfs/oe-sumo/20200521/${ROOTFS_MACHINE}"
  # Write kernel related variables
  create_kernel_vars_for_machine ${BUNDLED_URL}

  # Define LAVA job priority
  lava_job_priority="$(get_branch_priority "${GIT_BRANCH}" "${DEVICE_TYPE}" "${QA_PROJECT}")"
  export LAVA_JOB_PRIORITY=${lava_job_priority}

  # Redefine QA_TEAM if in production
  if [ "$(is_board_migrated "${QA_PROJECT}" "${DEVICE_TYPE}")" = "YES" ]; then
    QA_TEAM="lkft"
  fi

  # Write rootfs related variables
  create_rootfs_vars_for_machine ${BUNDLED_URL}

  # Write project specific variables
  project_specific_variables

  echo
  echo "---vvv------${TEST_PLANS_VAR_FILE}------vvv---"
  cat "${TEST_PLANS_VAR_FILE}"
  echo "---^^^------${TEST_PLANS_VAR_FILE}------^^^---"

  generate_submit_tests
fi
