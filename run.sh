#!/bin/bash

set -eu

# Requires:
#   $ROOT_DIR
#   $WORKDIR
#   $LIST_MACHINES
#   $BUILD_FILE
generate_tests() {
  for GIT_BRANCH in ${GIT_BRANCHES[*]}; do

    export REPO_NAME="stable-rc"
    case "$GIT_BRANCH" in
      mainline | next) REPO_NAME="${GIT_BRANCH}" ;;
    esac

    for ARCH in "${!LIST_MACHINES[@]}"; do
      for MACHINE in ${LIST_MACHINES[$ARCH]}; do
        echo
        echo "GIT_BRANCH=${GIT_BRANCH}; ARCH=${ARCH}; MACHINE=${MACHINE}"

        "${ROOT_DIR}/gen-variables.sh" "${BUILD_FILE}"
        bash "${WORKDIR}/submit-tests.sh"

        output_dir="tests/${GIT_BRANCH}/${ARCH}/${MACHINE}"
        mkdir -p "${output_dir}"
        cp -p "${WORKDIR}/variables.ini" "${output_dir}/"
        cp -p "${WORKDIR}/submit-tests.sh" "${output_dir}/"
        mv "${WORKDIR}/lava-test-plans/tmp/"* "${output_dir}/"
      done
    done
  done
}

source_environment() {
  set -a
  # shellcheck disable=SC1090
  . "${ENV_FILE}"
  set +a
}

ROOT_DIR="$(dirname "$(readlink -e "$0")")"
WORKDIR="${CI_PROJECT_DIR:-${ROOT_DIR}/workspace}"

ENV_FILE="${1:-${ROOT_DIR}/gitlab-env}"
BUILD_FILE="${2:-${ROOT_DIR}/gitlab-build.json}"

if [ ! -e "${ENV_FILE}" ]; then
  echo "Environment file (${ENV_FILE}) does not exist"
  exit 1
fi

if [ ! -e "${BUILD_FILE}" ]; then
  echo "Build info file (${BUILD_FILE}) does not exist"
  exit 1
fi

export DRY_RUN="--dry-run"

declare -A LIST_MACHINES
LIST_MACHINES["arm"]="qemu_arm am57xx-evm"
LIST_MACHINES["arm64"]="qemu_arm64 juno hikey dragonboard-410c ls2088ardb"
LIST_MACHINES["x86"]="qemu_x86_64 intel-corei7-64"
LIST_MACHINES["i386"]="qemu_i386 intel-core2-32"

declare -a GIT_BRANCHES
GIT_BRANCHES=(
  linux-4.4.y
  linux-4.9.y
  linux-4.14.y
  linux-4.19.y
  linux-5.4.y
  linux-5.6.y
  linux-5.7.y
  mainline
  next
)

rm -rf tests tests.orig tests.new "${WORKDIR}/lava-test-plans/tmp/"

source_environment
# Generate test jobs (YAML files) with merge_request applied
generate_tests
mv tests tests.new

if [ -v CI_MERGE_REQUEST_PROJECT_URL ]; then
  # Generate test jobs (YAML files) from branch point
  # From https://stackoverflow.com/a/4991675:
  git fetch "${CI_MERGE_REQUEST_PROJECT_URL}" refs/heads/master
  echo "diff --old-line-format='' --new-line-format='' <(git rev-list --first-parent FETCH_HEAD) <(git rev-list --first-parent HEAD) | head -1"
  PREV_REF=$(diff --old-line-format='' --new-line-format='' <(git rev-list --first-parent FETCH_HEAD) <(git rev-list --first-parent HEAD) | head -1)
  echo "Merge request: $(git rev-parse HEAD)"
  echo "Master branch: $(git rev-parse FETCH_HEAD)"
  echo "Branch point:  ${PREV_REF}"
  git checkout "${PREV_REF}"
  source_environment
  generate_tests
  mv tests tests.orig

  diff -aur tests.orig tests.new | tee tests.diff
fi
