#!/bin/bash

# This can take the printout of `env` from a Gitlab job
# and convert it into a local gitlab-env.sh that can be
# used for testing locally. It filters out a few
# variables that should be overwritten in the local
# environment (such as PATH, SHELL, SHLVL, USER, etc.).
#
# This script also guards the value of variables in cases
# where they there are spaces. Effectively, this means
# that the output of this script can be sourced locally
# without concerns.

FIRST=(
  ARCH
  MACHINE
)

# Please note that CI should only be defined in a CI
# loop. That is why we are commenting it out here.
COMMENTED=(
  CI
  HOME
  HOSTNAME
  LANG
  OLDPWD
  PATH
  PWD
  SHELL
  SHLVL
  SSH_CLIENT
  SSH_CONNECTION
  USER
  XDG_RUNTIME_DIR
  XDG_SESSION_ID
  _
)

CI_WHITELIST=(
  CI_BUILD_ID
  CI_PIPELINE_URL
)

if [ ! $# -ge 1 ]; then
  echo "Need a Gitlab \`env\` file as argument"
  exit 1
fi

# Put the most important information on top
for i in "${FIRST[@]}"; do
  grep "^${i}" "$1"
done
echo "#"

# Get the rest of the variables, alphabetically
while read -r l; do
  var="${l%%=*}"
  val="${l#*=}"
  comment=""

  # Have we printed it already?
  # shellcheck disable=SC2076
  if [[ " ${FIRST[*]} " =~ " ${var} " ]]; then
    continue
  fi

  # Should it be commented out?
  # shellcheck disable=SC2076
  if [[ " ${COMMENTED[*]} " =~ " ${var} " ]]; then
    comment="#"
  fi
  # shellcheck disable=SC2076
  if [[ "${var:0:3}" == "CI_" ]]; then
    if [[ ! " ${CI_WHITELIST[*]} " =~ " ${var} " ]]; then
      comment="#"
    fi
  fi
  echo "${comment}${var}=\"${val}\""

done < <(sort "$1" | grep -v -e "^$")
